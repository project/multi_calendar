-- SUMMARY --
The Multi_Calendar module Provide a new method to  display other date types(like Hijri , Jalali(shamsi ))
 without  require any patch .This module support multi calendar in multi language sites.
you can customize outpout format in desire date type.

-- REQUIREMENTS --
ctools
-- INSTALLATION --
 Install as usual, see http://drupal.org/node/70151 for further information.
 Go to admin/modules and enable multi calendar modules

-- CONFIGURATION --
For setting up Multi Calendar module go to 
admin/config/regional/multi_calendar and select format and type of your optional date
multi calendar support multi date type in multi language
Multi Calendar get its format from 
admin/config/regional/date-time
if you want customize date format you should add your custom format in
admin/config/regional/date-time/types/add


