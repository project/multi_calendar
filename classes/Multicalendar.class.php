<?php
/**
 * A helper class to handle multicalendar requires
 *
 * @author 
 */
class Multicalendar {
     private   $pre;
     private   $default;
     private   $calendars;
     
    function __construct() {
       $this->pre="Multi_calendar_";
       $this->default=  serialize(array(
           'format' => 'short',
           'popup_format'=>'Y/m/d',
           'type' => 'georgian',
           'range_max' => 50,
           'range_min' => 50,
           'theme' =>'calendar-system.css',
           'icon' => 'cal4',
             ));
      $this->calendars=array(
              'shamsi' => array(
                    'name' => t('shamsi'),
                    'path' => 'jcalendar.class.php', ),
             'ghamari' => array(
                    'name' => t('ghamari'),
                    'path' => 'uCal.class.php', ),
             'georgian' => array(
                    'name' => t('georgian'),
                    'path' => 'aaaa', ),
                );
    }
/**
 * Get saved variables in variable according the $lang
 * @param type $lang the langauge name
 * @return array 
 */
    public function get($lang) {
        //$active=$this->activelang_get();
        $var = unserialize(variable_get($this->pre.$lang, $this->default));
        return $var;
        
    }
/**
 *Set a variable in variable according langnauge and value passed
 * @param type $lang
 * @param type $value 
 */
    public function set($lang, $value) {
        variable_set($this->pre.$lang, $value);
    }
    
    
    public function activelang_get() {
      global $language;
      return ($language && $language->name) ? $language->name : 'default';
      
    }
/**
 *  retrive active languages
 * * @return
 *   An array of active languages.
 */
    public function languages_get() {
          global $language;
          $languages['default'] = array(
          'name' => $language->name
            );

          $locales = function_exists('locale_language_list') ?
          locale_language_list('name', TRUE) : NULL;

          if (!is_null($locales)) {
            foreach ($locales as $name => $title) 
            $languages[$name] = array('name' => $title );
            }
            return $languages;
      }
      
  /**
   * get date format of drupal system , short, medium , long and customs
   * @return type 
   */    
     public function dateformats_get()
            {
             $date_formats=array();
             $format=  _system_date_format_types_build();
             foreach($format as $name=>$value)
                    $date_formats[$name] = $name;
             return $date_formats;
            }

/**
 *get calendars 
 * @return an Array of exist calendars  
 */
   public function calendars_get(){
         foreach ($this->calendars as $cal => $name)
             $calendars[$cal]=$name['name'];
         return $calendars;
     }
/**
 * get date accroding to current calendar type and input timestamp
 * @param type $timestamp
 * @param type $format
 * @return a String of date in current calendar  
 */   
  public  function  date_get($timestamp, $format=FALSE){
        $lang=$this->activelang_get();
        $curformat=$this->get($lang);
        //in future should use local format
         $format=(!$format)?variable_get('date_format_'.$curformat['format'],'Y-m-d H:i'):$format;

        if($curformat['type']!='georgian')
             require_once drupal_get_path('module', 'multi_calendar') . '/classes/'.$this->calendars[$curformat['type']]['path'];
        switch ($curformat['type'])
          {
           case 'shamsi' :$c = new jCalendar;
                          $output=$c->date($format, $timestamp);   
                          $output=$this->persiandigit($output);
                          break;
           case 'ghamari' :$c = new uCal();
                          $output=$c->date($format, $timestamp,$hijri = 1);   
                          break;
           case 'georgian' :
                          $output=date($format, $timestamp);   
                          break;
          }
          
         return $output;
     }
 /**
  * 
  * @param type $string
  * @return boolean 
  */    
     public function date_set($string)
     {
         $checked=$this->check_format($string);
         if(!$checked)
             return FALSE;
         list($y,$m,$d,$h,$i)=$checked;
         $lang=$this->activelang_get();
         $curformat=$this->get($lang);
         if($curformat['type']!='georgian')
             require_once drupal_get_path('module', 'multi_calendar') . '/classes/'.$this->calendars[$curformat['type']]['path'];
         switch ($curformat['type'])
          {
           case 'shamsi' :$c = new jCalendar;
                          $timestamp=$c->mktime($h,$i,null,$m,$d,$y);
                          $output=date('Y-m-d H:i',$timestamp );  
                          break;
           case 'ghamari' :$c = new uCal();
                          $timestamp=$c->mktime($h,$i,null,$m,$d,$y);
                          $output=date('Y-m-d H:i',$timestamp );  
                          break;
           case 'georgian' :
                          $timestamp=mktime($h,$i,null,$m,$d,$y);
                          $output=date('Y-m-d H:i', $timestamp);   
                          break;
          }
          if(!$output)
              return false;
         return $output;  
         
     }
     
  /**
   *
   * @param type $str
   * @return type 
   */
     public function persiandigit($str,$show=FALSE) {
         if ($show) {
             $lang=$this->activelang_get();
             $curformat=$this->get($lang);
             if ($curformat['type'] == 'shamsi' || $curformat['type'] == 'ghamari' )
                return str_replace(array('0','1','2','3','4','5','6','7','8','9'), array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'), $str); 
             }
         else
		return str_replace( array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'),array('0','1','2','3','4','5','6','7','8','9'), $str);
	}
  /**
   *
   * @param type $string
   * @return boolean 
   */
    private function check_format($string)
    {
        $string=$this->persiandigit($string);
         $str=trim(preg_replace('/( )+/',' ',$string));
         $date_split=preg_split('/(-|:| )/',$str) + explode('-',date('Y-m-d-H-i'));//if user don't set hour and minute
         
         foreach($date_split as $i=>$val)
           { 
             if($date_split[$i] == '')
                 unset($date_split[$i]);

             if(!preg_match('/^[0-9]{1,}$/',$val))
                    return false;
           }
           if($date_split[3]>23 || $date_split[4]>59)
                return false;
        return $date_split;
    }   
    
  /**
   *
   * @param type $string
   * @return type 
   */
    public function multi_timstamp_get($string)
    {
        
         $checked=$this->check_format($string);
         if(!$checked)
             return time();
         
         list($y,$m,$d,$h,$i)=$checked;
         $lang=$this->activelang_get();
         $curformat=$this->get($lang);
         if($curformat['type']!='georgian')
             require_once drupal_get_path('module', 'multi_calendar') . '/classes/'.$this->calendars[$curformat['type']]['path'];
         switch ($curformat['type'])
          {
           case 'shamsi' :$c = new jCalendar;
                          $timestamp=$c->mktime($h,$i,null,$m,$d,$y);
                          break;
           case 'ghamari' :$c = new uCal();
                          $timestamp=$c->mktime($h,$i,null,$m,$d,$y);
                          break;
           case 'georgian' :
                          $timestamp=mktime($h,$i,null,$m,$d,$y);
                          break;
        }
        return $timestamp;
    }
    //put your code here
}

?>
